package kelompok.AbsenKantorService.repository;

import kelompok.AbsenKantorService.model.Absen;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AbsenRepository extends PagingAndSortingRepository<Absen, String> {

    @Query("SELECT E FROM Absen E WHERE absenId =:absenId")
    Absen getById(String absenId);

    @Query("SELECT E FROM Absen E WHERE tipe = 'MASUK' AND DATE(created) = current_date")
    Absen getAbsenMasukNow();

    @Query("SELECT E FROM Absen E WHERE tipe = 'KELUAR' AND DATE(created) = current_date")
    Absen getAbsenKeluarNow();

    @Query("SELECT E FROM Absen E WHERE mUserId =:mUserId")
    List<Absen> getBymUserId(String mUserId);
}
